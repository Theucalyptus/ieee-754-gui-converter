#Ce fichier continet les fonctions permettant l'encodage et le décodage de nombre réel
from math import floor

def encode(nbr_reel, double_pres):
    '''
    nbr_reel est un réel de type float
    double_pres est un booléen: True signifie encodage en 64bit, false en 32bit
    Renvoie l'écriture aproximative du reel en binaire d'après la norme IEEE 754
    '''

    signe = '0' if nbr_reel >=0 else '1' #détermine l'écriture binaire du signe du réel

    a, exposant = nbr_reel, 0 #détermine l'exposant
    a = -a if a < 0 else a

    while a > 1:
        a*=0.5
        exposant +=1

    while a < 1 and a != 0:
        a*=2
        exposant-=1


    exposant += 127 if double_pres == False else 1023 #décale en fonction du mode
    exposant = bin(exposant)#détermine l'écriture binaire de l'exposant
    exposant = exposant[2:len(exposant)]

    len_exp = 8 if double_pres == False else 11

    while len(exposant) < len_exp:
        exposant = '0' + exposant

    mantisse, i = str(), 0 #calcul  de la mantisse
    d = 52 if double_pres == True else 23 #determination de la longueur de mantisse voulue

    a = a-floor(a) #récupération de la partie décimal
    while a != 0:
        a = 2*a
        if a >=1:
            mantisse = mantisse + '1'
            a-=1
        else:
            mantisse = mantisse + '0'
        i+=1

    for x in range(0,d-i): #ajout de zero à la fin de la mantisse si manquants
        mantisse = mantisse + '0'

    return signe, exposant, mantisse[0:d]





def decode(signe, exposant, mantisse, double_pres):
    '''
        Convertie le reel binaire encodé selon la norme IEEE 754 en écriture décimal
        signe est de type string, contient un bit 0 ou 1
        exposant est de type string, mantisse est de type string
        doubles_pres est un booléen indiquant l'utilisation de 64bits
    '''

    try:
        int(signe, 2)
    except ValueError:
        return False, "Le signe"

    try:
        int(exposant, 2)
    except ValueError:
        return False, "L'exposant"

    try:
        int(mantisse, 2)
    except ValueError:
        return False, 'La mantisse'

    #Fin vérirication que les champs ne contient que des nombres binaires validesxc

    signe_ = 1 if signe == '0' else -1 #détermination du signe

    exposant = int(exposant, 2) - 127 if double_pres == False else int(exposant, 2) - 1023 #calcul de l'exposant

    nbr = 1 #lecture de la mantisse et convertion en décimal
    for x in range(len(mantisse)):
        nbr+=int(mantisse[x], 2) *(2**(-x-1))

    if nbr != 1.0:
        nbr*=2**exposant*signe_ #calcul du reel encodé

    return nbr, None #retour
