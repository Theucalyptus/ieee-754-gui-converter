﻿from tkinter import *
import utils

fenetre = Tk()
fenetre.title('Convertisseur Décimal-Binaire (IEEE 754)')

last_decimal, last_signe, last_exposant, last_mantisse, last_mode = str(), str(), str(), str(), False


def Update():
    '''
    Fonction principle de mise à jour du programme.
    Lance la conversion du réel en binaire et inversement en fonction des champs/mode ayant changés
    '''

    global last_decimal #acces aux valeurs globales
    global last_signe
    global last_exposant
    global last_mantisse
    global last_mode
    force = False
    error = False

    mode = False if str_mode_actif.get() == 'Mode Simple Précision (32bits)' else True #Récupéra tion du mode en cours d'utilisation / sélectionné
    if last_mode != mode:
        force = True
    last_mode = mode

    if last_decimal != decimal.get() or force == True : #Si l'utilisateur à modifier le réel décimal
         try: #vérification que le nombre saisie est correct
             float(decimal.get())
         except ValueError: #si non
             texte.set('Le nombre réel est invalide.')
             error = True

         if decimal.get() != '' and error != True: #si le nombre n'est pas vide et est valide, on lance la conversion décimal -> binaire
            a,b,c = utils.encode(float(decimal.get()), mode) #conversion
            signe.set(a), exposant.set(b), mantisse.set(c) #remplissage des champs avec le resultat
            last_decimal, last_signe, last_exposant, last_mantisse = decimal.get(), signe.get(), exposant.get(), mantisse.get() # mise à jour des variables de comparaison

            test = utils.decode(last_signe, last_exposant, last_mantisse, mode)[0] #reconversion de la valeur encodé
            if float(last_decimal) - float(test) == 0 and mode == False: #vérification exactitude de l'encodage
                texte.set("Encodage exact.")
            elif mode == False:
                texte.set("Encode approximatif : erreur = " + str(abs(float(last_decimal) - float(test)))) #calcul et affichage de la marge d'erreur
            else:
                texte.set('') #si en 64bits (le code n'étant pas assez précis pour détexter une erreur d'encodage sur les nombres 64bits avec cette méthode)

    elif last_signe != signe.get() or last_exposant != exposant.get() or last_mantisse != mantisse.get() or force == True: #Sinon si l'utilisateur à modifier
        if signe.get() != '' or exposant.get() != '' or mantisse.get() != '':
            last_decimal, x = utils.decode(signe.get(), exposant.get(), mantisse.get(), mode)
            if last_decimal == False: #Vérification de si la fonction à pu convertir avec succès : si non, x contient le nom de la valeure incorrecte.
                texte.set(x + ' est invalide.')
            else:
                texte.set('')
                decimal.set(last_decimal) #mise à jour des entries
            last_decimal, last_signe, last_exposant, last_mantisse = decimal.get(), signe.get(), exposant.get(), mantisse.get()

    fenetre.after(300, Update) #relance l'update plus tard pour le coté 'intéractif'


fenetre.geometry("500x300") #vérouillage de la géométrie de la fenètre

#Création du widget menu pour la sélection du mode souhaité
list_mode = ('Mode Simple Précision (32bits)', 'Mode Double Précision (64bits)')
str_mode_actif = StringVar()
str_mode_actif.set(list_mode[0])
selection_mode = OptionMenu(fenetre, str_mode_actif, *list_mode).pack()


#Déclaration des zone d'entrées
decimal, signe, exposant, mantisse = StringVar(), StringVar(), StringVar(), StringVar()

frame0 = Frame(fenetre)
frame0.pack(pady=50, anchor=CENTER)

frame1 = Frame(frame0)
frame1.pack(fill=X)
label_decimal = Label(frame1, text="Nombre décimal :",font=("Arial", 10, "bold")).pack(side=LEFT)
boite_texte_decimal = Entry(frame1, textvariable=decimal).pack(fill=X, side=RIGHT, padx=5, expand=TRUE)

frame2 = Frame(frame0)
frame2.pack(fill=X)
label_signe = Label(frame2, text="Signe :").pack(side=LEFT)
boite_texte_signe= Entry(frame2, textvariable=signe).pack(fill=X, side=RIGHT, padx=5, expand=TRUE)

frame3 = Frame(frame0)
frame3.pack(fill=X)
label_exposant = Label(frame3, text="Exposant :").pack(side=LEFT)
boite_texte_exposant= Entry(frame3, textvariable=exposant, width=12).pack(fill=X, side=RIGHT, padx=5, expand=TRUE)

frame4 = Frame(frame0)
frame4.pack(fill=X)
label_mantisse = Label(frame4, text="Mantisse :").pack(side=LEFT)
boite_texte_mantisse = Entry(frame4, textvariable=mantisse, width=54).pack(fill=X, side=RIGHT, padx=5, expand=TRUE)


#Création du texte d'information
texte = StringVar()
texte.set('')
encode_exact = Label(fenetre, textvariable=texte,font=("Comic sans MS", 10, "bold")).pack(anchor=S)

fenetre.after(500, Update)
fenetre.mainloop()